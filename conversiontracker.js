(function ($) {
  Drupal.behaviors.conversiontracker = {
    attach: function (context) {
      var settings = Drupal.settings.conversiontracker;
      if (settings && settings.selector && settings.trackers.length) {
        $(settings.selector, context).trackConversions(settings.trackers);
      }
    }
  };
})(jQuery);
