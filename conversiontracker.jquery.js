/**
 * Conversion tracking.
 *
 * Example:
 * $('a.buy').trackConversions([
 *   {
 *     service: 'facebook',
 *     id: 1234567890123,
 *     value: '1.23'
 *   },
 *   {
 *     service: 'adwords',
 *     id: 123456789,
 *     label: 'AbC_DEfGhIJKLmNOPqR',
 *     value: 0
 *   },
 *   {
 *     service: 'glispa',
 *     id: 12345,
 *     value: ''
 *   }
 * ]);
 */
(function($) {
  // Imitate the trackers' snippets.
  var scripts = {
    adwords: function(vars, done) {
      window.google_conversion_id = vars.id;
      window.google_conversion_language = 'en';
      window.google_conversion_format = '3';
      window.google_conversion_color = 'ffffff';
      window.google_conversion_label = vars.label;
      window.google_conversion_value = vars.value;
      $.getScript('//www.googleadservices.com/pagead/conversion.js', done);
    },
    facebook: function(vars, done) {
      window.fb_param = {};
      window.fb_param.pixel_id = vars.id;
      window.fb_param.value = vars.value;
      $.getScript('//connect.facebook.net/en_US/fp.js', done);
    },
    glispa: function(vars, done) {
      // Load the tracking pixel directly, as that's all the script does anyway.
      // Wait untill the image is fully loaded. This won't work in all browsers,
      // but there's the fallback redirect after 2 seconds to handle that.
      var img = new Image();
      img.src = '//ads.glispa.com/track_lead/' + vars.id + '/' + vars.value;
      $(img).bind('load', done);
    }
  }

  /**
   * Tracks a conversion when selected elements are clicked.
   */
  $.fn.trackConversions = function(trackers) {
    // Normalize trackers' options.
    for (var i = 0, len = trackers.length; i < len; i++) {
      trackers[i].id = trackers[i].id.toString();
      if (trackers[i].service == 'adwords') {
        trackers[i].value = (trackers[i].value || '0').toString();
        trackers[i].label = (trackers[i].label || '').toString();
      }
      else if (trackers[i].service == 'facebook') {
        trackers[i].value = (trackers[i].value || '0.00').toString();
      }
      else if (trackers[i].service == 'glispa') {
        trackers[i].value = (trackers[i].value || '').toString();
      }
    }

    // Bind to the click event of each selected element.
    return this.each(function() {
      var self = this;
      var data = $(self).data();

      $(self).bind('click', function(event) {
        var completed = 0;

        // Redirects to the link element's URL.
        var redirect = function() {
          window.location = self.href;
        };

        // Load tracking scripts for the specified trackers.
        for (var i = 0, len = trackers.length; i < len; i++) {
          var vars = trackers[i];

          // Allow the tracker's value to be overridden by the <a> tag.
          if (data[vars.service + '-value'] !== undefined) {
            vars = $.extend({}, trackers[i]);
            vars.value = data[vars.service + '-value'];
          }

          // Load all scripts, redirecting once they are all done. Redirect
          // immediately if it's only Glispa, otherwise use a short delay to
          // make sure the scripts get to do their actual tracking.
          scripts[vars.service](vars, function() {
            if (len == 1 && vars.service == 'glispa') {
              redirect();
            }
            else if (++completed == len) {
              setTimeout(redirect, 500);
            }
          });
        }

        // If all scripts aren't done after 2 seconds, timeout and redirect.
        setTimeout(redirect, 2000);

        return false;
      });
    });
  };
})(jQuery);
