<?php

/**
 * @file
 * Administrative page callbacks for the conversiontracker module.
 */

/**
 * Settings form.
 */
function conversiontracker_admin_settings_form($form_state) {
  // Adwords.
  $form['adwords'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google AdWords'),
    '#description' => t("The values for these fields can be found on the Google AdWords website under <a href=\"!url\">Tools and Analysis › Conversions</a>. Click on a conversion's name, select the <em>Code</em> tab and then select <em>I make changes to the code</em>. Copy the individual values found in the tracking code into their corresponding fields below. You only need the ID, value and label.", array('!url' => 'https://adwords.google.com/o/ConversionTracking/Manager')),
  );

  $form['adwords']['conversiontracker_adwords_id'] = array(
    '#type' => 'textfield',
    '#title' => t('ID'),
    '#description' => t('The value of <em>google_conversion_id</em> in the code.'),
    '#default_value' => variable_get('conversiontracker_adwords_id', ''),
    '#size' => 10,
  );

  $form['adwords']['conversiontracker_adwords_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value of <em>google_conversion_value</em> in the code (defaults to <em>0</em>).'),
    '#default_value' => variable_get('conversiontracker_adwords_value', '0'),
    '#size' => 3,
    '#maxlength' => 3,
  );

  $form['adwords']['conversiontracker_adwords_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The value of <em>google_conversion_label</em> in the code.'),
    '#default_value' => variable_get('conversiontracker_adwords_label', ''),
  );

  $form['adwords']['conversiontracker_adwords_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('conversiontracker_adwords_enabled', FALSE),
  );

  // Facebook.
  $form['facebook'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook'),
    '#description' => t("The values for these fields can be found in Facebook Ads Manager under <a href=\"!url\">Conversion Tracking</a> (first select the account). Click on <em>View Pixel Code</em> for the conversion, and copy the individual values found in the code into their corresponding fields below.", array('!url' => 'https://www.facebook.com/ads/manage/accounts.php')),
  );

  $form['facebook']['conversiontracker_facebook_id'] = array(
    '#type' => 'textfield',
    '#title' => t('ID'),
    '#description' => t('The value of <em>fb_param.pixel_id</em> in the code.'),
    '#default_value' => variable_get('conversiontracker_facebook_id', ''),
    '#size' => 13,
  );

  $form['facebook']['conversiontracker_facebook_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value of <em>fb_param.value</em> in the code (defaults to <em>0.0</em>).'),
    '#default_value' => variable_get('conversiontracker_facebook_value', '0.0'),
    '#size' => 3,
    '#maxlength' => 3,
  );

  $form['facebook']['conversiontracker_facebook_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('conversiontracker_facebook_enabled', FALSE),
  );

  // Glispa.
  $form['glispa'] = array(
    '#type' => 'fieldset',
    '#title' => t('Glispa'),
    '#description' => t('Glispa lead tracking.'),
  );

  $form['glispa']['conversiontracker_glispa_id'] = array(
    '#type' => 'textfield',
    '#title' => t('ID'),
    '#description' => t('This is your campaign ID within the glispa system. A glispa representative will send you the pixel with the correct value already inserted.'),
    '#default_value' => variable_get('conversiontracker_glispa_id', ''),
    '#size' => 5,
  );

  $form['glispa']['conversiontracker_glispa_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('This is a unique value that you generate for each conversion or sign-up. This could be an order ID, member ID, transaction ID, or anything that uniquely identifies the specific conversion/lead/sign-up in your system.'),
    '#default_value' => variable_get('conversiontracker_glispa_value', ''),
    '#size' => 3,
    '#maxlength' => 3,
  );

  $form['glispa']['conversiontracker_glispa_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('conversiontracker_glispa_enabled', FALSE),
  );

  // Selector.
  $form['conversiontracker_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Selector'),
    '#description' => t('A jQuery/CSS style selector of the links on which to track conversions when clicked. For example, to select all links with class <em>store</em>, enter ".store" (the leading dot is important).'),
    '#default_value' => variable_get('conversiontracker_selector', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
